import { HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DataProvider } from '../../providers/data/data';


@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  constructor(public navCtrl: NavController,private dataProvider: DataProvider, public navParams: NavParams, private iab: InAppBrowser) {
    setTimeout(() => {
      this.openWebView()
    }, 1200);
  }

  ionViewDidLoad() {
  }

  openWebView() {
    const app = this.iab.create('http://admin.ekhiaboon.com/', '_self', { location: 'no' });

    app.show();
  }
}
