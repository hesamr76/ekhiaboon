import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/timeout';

import { DataProvider } from '../data/data';

const baseURL = 'http://admin.ekhiaboon.com/api/ekhiaboon'

// const APP_VERSION = '1.0.0' //there is another in app.component and faction.ts

@Injectable()
export class ApiProvider {
  
  constructor(
    public http: HttpClient,
    private dataProvider: DataProvider,
  ) {
  }

  get(method, completion, errorHandler?) {
    let headers = new HttpHeaders();

    let authorization = 'Basic'
    if(this.dataProvider.getAuth()){
      authorization = this.dataProvider.getAuth()
    }
    headers = headers.append('Authorization', authorization)

    this.http.get(baseURL + method, { headers: headers })
      .timeout(10000)
      .subscribe(
        data  => completion(data),
        error => errorHandler ? errorHandler(error.status, method, completion) : this.error( error.status, method, null, completion )
      );
  }

  async post(method, body, completion, errorHandler=null) {
    let headers = new HttpHeaders();

    let authorization = 'Basic'    
    if(this.dataProvider.getAuth()){
      authorization = this.dataProvider.getAuth()
    }
    headers = headers.append('Authorization', authorization); 

    this.http.post(baseURL + method, body, { headers: headers })
      .timeout(10000)
      .subscribe( 
        data => completion(data), 
        error => errorHandler ? errorHandler() : this.error( error.status, method, body, completion )
      );
  }

  error(errorCode, methode, body, completion){
  
    switch(errorCode){
      
      default:
        console.log('bad network!!');
        
        break   
    }
  }

}