import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';

import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Storage } from '@ionic/storage';


@Injectable()
export class DataProvider {

  auth = null
  constructor(private nativeStorage: NativeStorage, private storage: Storage, private platform: Platform) {
  }

  setAuth(token = null){
    this.auth = token
    if (this.platform.is('cordova')) {
      this.nativeStorage.setItem('token', token)
      .then(
        () => console.log('Stored item!'),
        error => console.error('Error storing item', error)
      );
    }
    this.storage.set("token", token)
  }

  getAuth(){
    if (!this.auth) {
      if (this.platform.is('cordova')) {
        this.nativeStorage.getItem('token')
        .then(
          data =>
            {
              this.auth = data 
              return this.auth
            }
        )
      } else {
        this.storage.get("token").then(data => {
          this.auth = data 
          return this.auth
        }) 
      }
    }else{
      return this.auth
    }
  }
}
