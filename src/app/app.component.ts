import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Storage } from '@ionic/storage';

import { ApiProvider } from '../providers/api/api';
import { DataProvider } from '../providers/data/data';
// import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = DashboardPage;
  @ViewChild(Nav) nav: Nav;

  constructor(
    private platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private apiProvider: ApiProvider,
    private dataProvider: DataProvider,
    private nativeStorage: NativeStorage,
    private storage: Storage) {
    platform.ready().then(() => {
      console.log('Platform is ready!');

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      // this.splashScreenHide();
      
      
      // if (this.platform.is('cordova')) {
      //   this.nativeStorage.getItem('token')
      //     .then(
      //       data => this.open(data),
      //       error => {
      //           this.storage.get("token").then(val => {
      //             this.open(val)        
      //           })
      //       }
      //     );
      // } else {
      //   this.storage.get("token").then(val => {
      //     this.open(val)        
      //   })
      // }
        
    });
  }

  splashScreenHide(){
    const splash = document.getElementById('splash-screen')
    splash.style.opacity = '0'
    setTimeout(() => { splash.remove() }, 300)
  }

  // open(token){
  //   let register = false

  //   if (token) {
  //     this.dataProvider.setAuth(token)
  //   } else {
  //     register = true
  //   }

  //   if (register) {
  //     this.nav.setRoot('DashboardPage')
  //   } else {
  //     this.nav.setRoot('DashboardPage')
  //   }
  // }
}

